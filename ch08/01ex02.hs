-- file: ch08/01ex02.hs


module GlobRegex
    (
      globToRegex
    , matchesGlob
    ) where


import Data.Char (isLetter, toLower, toUpper)
import Text.Regex.Posix ((=~))


data CaseSensitivity
    = CaseIgnore
    | CaseMatch
    deriving (Eq, Show)


globToRegex :: String -> CaseSensitivity -> String
globToRegex glob csens = '^' : globToRegex' glob csens ++ "$"


globToRegex' :: String -> CaseSensitivity -> String
globToRegex' glob csens =
    case glob of
        "" ->
            ""

        ('*':cs) ->
            ".*" ++ globToRegex' cs csens

        ('?':cs) ->
            '.' : globToRegex' cs csens

        ('[':'!':c:cs) ->
            "[^" ++ escape c csens ++ charClass cs csens

        ('[':c:cs) ->
            '[' : escape c csens ++ charClass cs csens

        ('[':_) ->
            error "unterminated character class"

        (c:cs) ->
            escape c csens ++ globToRegex' cs csens


escape :: Char -> CaseSensitivity -> String
escape c csens
    | c `elem` regexChars = '\\' : [c]
    | otherwise           = charCase c csens
    where
        regexChars = "\\+()^$.{}]|"


charClass :: String -> CaseSensitivity -> String
charClass (']':cs) csens = ']' : globToRegex' cs csens
charClass (c:cs)   csens = escape c csens ++ charClass cs csens
charClass []       _     = error "unterminated character class"


charCase:: Char -> CaseSensitivity -> String
charCase c csens
    | csens == CaseIgnore && isLetter c = ['(', toLower c, '|', toUpper c, ')']
    | otherwise                         = [c]


matchesGlob :: FilePath -> String -> CaseSensitivity -> Bool
matchesGlob name pat csens = name =~ globToRegex pat csens
