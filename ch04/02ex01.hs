-- file: ch04/02ex01.hs


import Data.Char (digitToInt)
import Data.List (foldl')


asInt_fold :: String -> Int
asInt_fold xs = foldl' step 0 xs
    where
        step acc d = acc * 10 + digitToInt d
