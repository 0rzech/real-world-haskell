-- file: ch04/lists.hs

import Data.Char (isSpace)

length' :: [a] -> Int
length' []     = 0
length' (x:xs) = len 1 xs
    where
        len l []     = l
        len l (y:ys) = len (l + 1) ys


null' :: [a] -> Bool
null' [] = True
null' _  = False


head' :: [a] -> a
head' []    = error "empty list"
head' (x:_) = x


tail' :: [a] -> [a]
tail' []     = error "empty list"
tail' (_:xs) = xs


last' :: [a] -> a
last' []     = error "empty list"
last' [x]    = x
last' (x:xs) = last' xs


init' :: [a] -> [a]
init' []     = error "empty list"
init' [x]    = []
init' (x:xs) = x : init' xs


(+++) :: [a] -> [a] -> [a]
xs +++ []     = xs
[] +++ ys     = ys
(x:xs) +++ ys = x : xs +++ ys


concat' :: [[a]] -> [a]
concat' [] = []
concat' (x:xs) = x +++ concat' xs


reverse' :: [a] -> [a]
reverse' []     = []
reverse' (x:xs) = reverse' xs +++ [x]


and' :: [Bool] -> Bool
and' []     = True
and' (x:xs) = x && and' xs


or' :: [Bool] -> Bool
or' []      = False
or' (x:xs)  = x || or' xs


all' :: (a -> Bool) -> [a] -> Bool
all' _ []     = True
all' p (x:xs) = p x && all' p xs

any' :: (a -> Bool) -> [a] -> Bool
any' _ []     = False
any' p (x:xs) = p x || any' p xs


take' :: Int -> [a] -> [a]
take' _ []     = []
take' n (x:xs) = if n > 0 then x : take' (n - 1) xs else []


drop' :: Int -> [a] -> [a]
drop' _ [] = []
drop' n (_:xs) = if n > 0 then drop' (n - 1) xs else xs


splitAt' :: Int -> [a] -> ([a], [a])
splitAt' _ [] = ([], [])
splitAt' n xs = spl n ([], xs)
    where
        spl n (ys, [])          = (reverse ys, [])
        spl n (ys, rest@(z:zs)) = if n > 0 then spl (n - 1) (z:ys, zs) else (reverse ys, rest)


takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' _ []     = []
takeWhile' p (x:xs) = if p x then x : takeWhile' p xs else []

dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' _ []          = []
dropWhile' p rest@(x:xs) = if p x then dropWhile' p xs else rest


span' :: (a -> Bool) -> [a] -> ([a], [a])
span' _ [] = ([], [])
span' p xs = spn p ([], xs)
    where
        spn p (ys, [])          = (reverse ys, [])
        spn p (ys, rest@(z:zs)) = if p z then spn p (z:ys, zs) else (reverse ys, rest)


break' :: (a -> Bool) -> [a] -> ([a], [a])
break' _ [] = ([], [])
break' p xs = brk p ([], xs)
    where
        brk p (ys, [])          = (reverse ys, [])
        brk p (ys, rest@(z:zs)) = if not (p z) then brk p (z:ys, zs) else (reverse ys, rest)


elem' :: Eq a => a -> [a] -> Bool
elem' _ [] = False
elem' e (x:xs) = if e == x then True else elem' e xs

notElem' :: Eq a => a -> [a] -> Bool
notElem' x xs = not (elem x xs)


filter' :: (a -> Bool) -> [a] -> [a]
filter' _ [] = []
filter' p (x:xs) = if p x then x : filter' p xs else filter' p xs


isPrefixOf' :: Eq a => [a] -> [a] -> Bool
isPrefixOf' [] _          = True
isPrefixOf' _ []          = False
isPrefixOf' (x:xs) (y:ys) = if x == y then isPrefixOf' xs ys else False


isInfixOf' :: Eq a => [a] -> [a] -> Bool
isInfixOf' [] _  = True
isInfixOf' _ []  = False
isInfixOf' xs ys = isInf xs ys
    where
        isInf [] _ = True
        isInf _ [] = False
        isInf (a:as) (b:bs) = if a == b then isInf as bs else isInfixOf' xs bs


isSuffixOf' :: Eq a => [a] -> [a] -> Bool
isSuffixOf' xs ys = xs == drop len ys
    where
        len = length ys - length xs


zip' :: [a] -> [b] -> [(a, b)]
zip' xs []         = []
zip' [] ys         = []
zip' (x:xs) (y:ys) = (x, y) : zip' xs ys


zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ xs []         = []
zipWith' _ [] ys         = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys


words' :: [Char] -> [[Char]]
words' [] = []
words' cs = wrd (reverse cs) [] []
    where
        wrd [] word res     = if null word then res else word:res
        wrd (x:xs) word res =
            if isSpace x
                then wrd xs [] (if null word then res else word:res)
                else wrd xs (x:word) res


unwords' :: [[Char]] -> [Char]
unwords' []     = []
unwords' (w:[]) = w
unwords' (w:ws) = w ++ " " ++ unwords' ws
