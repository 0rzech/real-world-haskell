-- file ch04/02ex06.hs

concat' :: [[a]] -> [a]
concat' = foldr (++) []
