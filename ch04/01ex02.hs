-- file: ch04/01ex02.hs

splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith _ [] = []
splitWith p cs = spl (reverse cs) [] []
    where
        spl [] word res     = if null word then res else word:res
        spl (x:xs) word res =
            if not (p x)
                then spl xs [] (if null word then res else word:res)
                else spl xs (x:word) res
