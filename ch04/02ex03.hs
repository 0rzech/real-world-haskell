-- file: ch04/02ex03.hs


import Data.Char (digitToInt, isDigit)
import Data.List (foldl')


asInt_fold :: String -> Int
asInt_fold xs =
    case xs of
        "" ->
            error "empty string"

        "-" ->
            error "not a number"

        '-':ys ->
            (-1) * foldl' step 0 ys

        _ ->
            foldl' step 0 xs

    where
        step acc d =
            if isDigit d
                then
                    let accumulated = acc * 10 + digitToInt d
                    in
                        if acc > accumulated
                            then error "Int boundary exceeded"
                            else accumulated
                else error "not a number"
