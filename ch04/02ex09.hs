-- file ch04/02ex09.hs


groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' p = foldr step []
    where
        step x []                 = [[x]]
        step x (gr@(y:group):acc) = if p x y then (x:gr):acc else [x]:gr:acc
