-- file: ch04/02ex02.hs


import Data.Char (digitToInt)
import Data.List (foldl', isPrefixOf)


asInt_fold :: String -> Int
asInt_fold xs =
    if "-" `isPrefixOf` xs
        then (-1) * foldl' step 0 (tail xs)
        else foldl' step 0 xs
    where
        step acc d = acc * 10 + digitToInt d
