-- file: ch04/01ex03.hs


import Data.Char (isAlphaNum)
import System.Environment (getArgs)


interactWith function inputFile outputFile =
    do  input <- readFile inputFile
        writeFile outputFile (function input)


main = mainWith myFunction
    where
        mainWith function =
            do  args <- getArgs
                case args of
                    [input, output] ->
                        interactWith function input output
                    
                    _ ->
                        putStrLn "error: exactly two arguments needed"
        
        myFunction = firstWords


firstWords :: String -> String
firstWords [] = []
firstWords xs = unlines (map fw (lines xs))
    where
        fw ys = takeWhile isAlphaNum ys
