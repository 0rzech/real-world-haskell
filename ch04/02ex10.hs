-- file ch04/02ex10.hs


import Data.Char (isSpace)
import Data.List (foldl')


anyl :: (a -> Bool) -> [a] -> Bool
anyl p = foldl' (\acc x -> acc || p x) False


anyr :: (a -> Bool) -> [a] -> Bool
anyr p = foldr (\x acc -> acc || p x) False


cycler :: [a] -> [a]
cycler = foldr (\x acc -> x ++ acc) [] . repeat


wordsl :: String -> [String]
wordsl s =
    let wrds = foldl' step [] s
    in reverse (reverse (head wrds) : tail wrds)
    where
        step [] c         = if isSpace c then [] else [[c]]
        step (word:acc) c = if isSpace c then []:(reverse word):acc else (c:word):acc

wordsr :: String -> [String]
wordsr = foldr step []
    where
        step c []         = if isSpace c then [] else [[c]]
        step c (word:acc) = if isSpace c then []:word:acc else (c:word):acc


unlinesl :: [String] -> String
unlinesl = foldl' (\acc l -> acc ++ l ++ "\n") []


unlinesr :: [String] -> String
unlinesr = foldr (\l acc -> l ++ "\n" ++ acc) []
