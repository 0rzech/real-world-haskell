-- file: ch04/02ex04.hs


import Data.Char (digitToInt, isDigit)
import Data.List (foldl')


type ErrorMessage = String


asInt_either :: String -> Either ErrorMessage Int
asInt_either xs =
    case xs of
        "" ->
            Left "empty string"

        "-" ->
            Left "not Int"

        '-':ys ->
            toNegative (foldl' step (Right 0) ys)

        _ ->
            foldl' step (Right 0) xs

    where
        step (Right acc) d =
            if isDigit d
                then
                    let accumulated = acc * 10 + digitToInt d
                    in
                        if acc > accumulated
                            then Left "Int boundary exceeded"
                            else Right accumulated
                else Left "not Int"
        step errorMsg _    = errorMsg

        toNegative (Right number) = Right ((-1) * number)
        toNegative errorMsg       = errorMsg
