-- file ch04/01ex01.hs


safeHead :: [a] -> Maybe a
safeHead []    = Nothing
safeHead (x:_) = Just x


safeTail :: [a] -> Maybe [a]
safeTail []     = Nothing
safeTail (_:xs) = Just xs


safeLast :: [a] -> Maybe a
safeLast []     = Nothing
safeLast [x]    = Just x
safeLast (_:xs) = safeLast xs


safeInit :: [a] -> Maybe [a]
safeInit []     = Nothing
safeInit [x]    = Just []
safeInit (x:xs) = Just (x : init' xs)
    where
        init' [x]    = []
        init' (x:xs) = x : init' xs
