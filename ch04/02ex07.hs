-- file ch04/02ex07.hs

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' _ []     = []
takeWhile' p (x:xs) = if p x then x : takeWhile' p xs else []

takeWhile'' :: (a -> Bool) -> [a] -> [a]
takeWhile'' p = foldr (\x acc -> if p x then x:acc else []) []
