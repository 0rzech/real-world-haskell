-- file: ch01/ex04.hs

main = interact wordCount
    where
        wordCount input = show (length (concat (words input))) ++ "\n"
