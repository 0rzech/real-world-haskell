-- file: ch02/ex02.hs

-- "naive" approach - results in "non-exhaustive patterns" exception
--lastButOne :: [a] -> a
--lastButOne (x:xs) = if length xs == 1 then x else lastButOne xs

-- errorproof approach
lastButOne :: [a] -> Maybe a
lastButOne []       = Nothing
lastButOne ([x])    = Nothing
lastButOne (x:y:[]) = Just x
lastButOne (x:xs)   = lastButOne xs
