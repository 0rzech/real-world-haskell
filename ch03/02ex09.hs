-- file: ch03/02ex09.hs

type Point
    = (Double, Double)


data Direction
    = L -- Left
    | R -- Right
    | S -- Straight
    deriving (Show)
