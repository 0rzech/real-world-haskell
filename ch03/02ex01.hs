-- file: ch03/02ex01.hs

length' []     = 0
length' (x:xs) = len xs 1
    where
        len [] l     = l
        len (x:xs) l = len xs (l + 1)
