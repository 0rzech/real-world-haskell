-- file: ch03/02ex02.hs

length' :: [a] -> Int
length' []     = 0
length' (x:xs) = len xs 1
    where
        len [] l     = l
        len (x:xs) l = len xs (l + 1)
