-- file: ch03/01ex02.hs

data Tree a
    = Node a (Maybe (Tree a)) (Maybe (Tree a))
    deriving (Show)
-- Example:
-- Node "parent" (Just (Node "left" Nothing Nothing)) (Just (Node "right" Nothing Nothing))
