-- file: ch03/02ex12.hs

import Data.List


type Point
    = (Double, Double)


data Direction
    = L -- Left
    | R -- Right
    | S -- Straight
    deriving (Eq, Show)


turn :: Point -> Point -> Point -> Direction
turn (x1, y1) (x2, y2) (x3, y3)
    | crossProduct > 0 = L
    | crossProduct < 0 = R
    | otherwise        = S
    where
        crossProduct = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)


turns :: [Point] -> [Direction]
turns points =
    case points of
        [] ->
            []
        
        (_:[]) ->
            []
        
        (_:_:[]) ->
            []
        
        (p1:p2:p3:ps) ->
            turn p1 p2 p3 : turns (p2:p3:ps)


sortByCoord :: [Point] -> [Point]
sortByCoord points = sortBy (sort) points
    where
        sort a b
            | snd a > snd b = GT
            | snd a < snd b = LT
            | otherwise     = fst a `compare` fst b


sortByAngle :: [Point] -> [Point]
sortByAngle []         = []
sortByAngle (p:points) = p : sortBy (sort) points
    where
        sort a b
            | tanA < tanB           = LT
            | tanA > tanB           = GT
            | distanceA < distanceB = LT
            | distanceA > distanceB = GT
            | otherwise = EQ
            where
                tanA = atan2 (snd a - snd p) (fst a - fst p)
                tanB = atan2 (snd b - snd p) (fst b - fst p)
                distanceA = distance p a
                distanceB = distance p b
                distance a b = sqrt (((fst b - fst a) ^ 2) + ((snd b - snd a) ^ 2))


grahamScan :: [Point] -> [Point]
grahamScan points =
    case points of
        [] ->
            []
        
        (_:[]) ->
            []
        
        (_:_:[]) ->
            []
        
        _ ->
            scan (drop 2 pts) (reverse (take 2 pts))
        
    where
        scan [] res             = reverse res
        scan (p:ps) (r1:r2:res)
            | turn r2 r1 p == L = scan ps (p:r1:r2:res)
            | null res          = scan ps (r1:r2:res)
            | otherwise         = scan (p:ps) (r2:res)
        
        pts = sortByAngle (sortByCoord points)
