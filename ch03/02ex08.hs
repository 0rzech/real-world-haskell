-- file: ch03/02ex08.hs

data Tree a
    = Node a (Tree a) (Tree a)
    | Empty
    deriving (Show)

height :: Tree a -> Int
height Empty        = 0
height (Node a l r) = 1 + max (height l) (height r)
