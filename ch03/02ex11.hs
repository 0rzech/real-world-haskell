-- file: ch03/02ex11.hs

type Point
    = (Double, Double)


data Direction
    = L -- Left
    | R -- Right
    | S -- Straight
    deriving (Show)


turn :: Point -> Point -> Point -> Direction
turn (x1, y1) (x2, y2) (x3, y3)
    | crossProduct > 0 = L
    | crossProduct < 0 = R
    | otherwise        = S
    where
        crossProduct = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)


turns :: [Point] -> [Direction]
turns points =
    case points of
        [] ->
            []
        
        (p:[]) ->
            []
        
        (p1:p2:[]) ->
            []
        
        (p1:p2:p3:ps) ->
            turn p1 p2 p3 : turns (p2:p3:ps)
