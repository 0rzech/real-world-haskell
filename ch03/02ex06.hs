-- file: ch03/02ex06.hs

import Data.List (sortBy)

-- ascending order
sortSubs :: [[a]] -> [[a]]
sortSubs xs = sortBy len xs
    where 
        len as bs
            | lenAs > lenBs = GT
            | lenAs < lenBs = LT
            | otherwise     = EQ
            where
                lenAs = length as
                lenBs = length bs
