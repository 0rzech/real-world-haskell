-- file: ch03/02ex05.hs

isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome xs = take (half + rest) xs == (reverse $ drop half xs)
    where
        half = len `div` 2
        rest = len `mod` 2
        len  = length xs
