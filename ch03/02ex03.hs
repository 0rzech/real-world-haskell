-- file: ch03/02ex03.hs

-- I'mma do it all myself:
mean :: Fractional a => [a] -> Maybe a
mean []     = Nothing
mean (x:xs) = Just (mean' x xs 1)
    where
        mean' m [] l     = m / l
        mean' m (x:xs) l = mean' (m + x) xs (l + 1)

-- I'll let libraries do it for me: 
--mean :: Fractional a => [a] -> Maybe a
--mean xs = if len > 0 then Just (sum xs / fromIntegral len) else Nothing
--    where len = length xs
