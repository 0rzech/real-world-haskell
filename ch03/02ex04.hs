-- file: ch03/02ex04.hs

palindrome :: [a] -> [a]
palindrome [] = []
palindrome xs = xs ++ reverse xs
