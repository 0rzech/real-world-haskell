-- file ch05/01ex01.hs


data Doc
    = Empty
    | Char Char
    | Text String
    | Line
    | Concat Doc Doc
    | Union Doc Doc
    deriving (Show, Eq)


punctuate :: Doc -> [Doc] -> [Doc]
punctuate p []     = []
punctuate p [d]    = [d]
punctuate p (d:ds) = (d <> p) : punctuate p ds


empty :: Doc
empty = Empty


char :: Char -> Doc
char c = Char c


text :: String -> Doc
text "" = Empty
text s  = Text s


double :: Double -> Doc
double d = text (show d)


line :: Doc
line = Line


(<>) :: Doc -> Doc -> Doc
x <> Empty = x
Empty <> y = y
x <> y     = x `Concat` y


hcat :: [Doc] -> Doc
hcat = fold (<>)


fold :: (Doc -> Doc -> Doc) -> [Doc] -> Doc
fold f = foldr f empty


fsep :: [Doc] -> Doc
fsep = fold (</>)


(</>) :: Doc -> Doc -> Doc
x </> y = x <> softLine <> y


softLine :: Doc
softLine = group line


group :: Doc -> Doc
group x = flatten x `Union` x


flatten :: Doc -> Doc
flatten (x `Concat` y) = flatten x `Concat` flatten y
flatten Line           = Char ' '
flatten (x `Union` _)  = flatten x
flatten other          = other


compact :: Doc -> String
compact x = transform [x]
    where
        transform []     = ""
        transform (d:ds) =
            case d of
                Empty ->
                    transform ds

                Char c ->
                    c : transform ds

                Text s ->
                    s ++ transform ds

                Line ->
                    '\n' : transform ds

                a `Concat` b ->
                    transform (a:b:ds)

                _ `Union` b ->
                    transform (b:ds)


pretty :: Int -> Doc -> String
pretty width x = best 0 [x]
    where
        best col (d:ds) =
            case d of
                Empty ->
                    best col ds

                Char c ->
                    c : best (col + 1) ds

                Text s ->
                    s ++ best (col + length s) ds

                Line ->
                    '\n' : best 0 ds

                a `Concat` b ->
                    best col (a:b:ds)

                a `Union` b ->
                    nicest col (best col (a:ds)) (best col (b:ds))
        best _ _        = ""

        nicest col a b
            | (width - least) `fits` a = a
            | otherwise                = b
            where
                least = min width col


fits :: Int -> String -> Bool
w `fits` _ | w < 0 = False
_ `fits` ""        = True
_ `fits` ('\n':_)  = True
w `fits` (c:cs)    = (w - 1) `fits` cs


fill :: Int -> Doc -> Doc
fill width x = widen 0 [] [x]
    where
        widen col line []     = finishLine col line False
        widen col line (d:ds) =
            case d of
                Empty ->
                    widen col (Empty:line) ds

                Char c ->
                    widen (col + 1) ((Char c):line) ds

                Text s ->
                    widen (col + length s) ((Text s):line) ds

                Line ->
                    finishLine col line True <> widen 0 [] ds

                a `Concat` b ->
                    widen col line (a:b:ds)

                a `Union` b ->
                    widen col line (a:ds) `Union` widen col line (b:ds)

        finishLine c l ln =
            Text (take (width - c) $ repeat ' ') <> hcat (reverse l) <> if ln then Line else Empty
